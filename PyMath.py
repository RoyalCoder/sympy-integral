from sympy import *
init_printing(use_unicode=False, wrap_line=False)
x = Symbol('x')
integrate((x**2)+(2*x))
integrate(sin(log(x, exp(1))), x)
integrate(1/(1+sin(x)+cos(x)), x)
integrate( log(x,exp(1)) / x*(1-log(x,exp(1))**2) ,x)
integrate( 1/exp(-x)*(4+9*exp(2*x)**(1/2)) ,x)
